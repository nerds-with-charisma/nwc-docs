import React from 'react';
import { PropTypes } from 'prop-types';

const CoverBackground = ({
  backgroundColor,
  children,
  minHeight,
  mp4,
  src,
  theme,
  webm,
}) => {
  const backgroundStyle = {
    backgroundImage: `url(${src})`,
    backgroundColor,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backroundPosition: 'center',
    overflow: 'hidden',
  };
  return (
    <div
      id="nwc--cover-background"
      className={theme}
      style={{ ...backgroundStyle, minHeight }}
    >
      {(webm || mp4) && (
        <video
          id="nwc--video"
          playsInline
          autoPlay
          muted
          loop
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            minWidth: '100%',
            minHeight: '100%',
            width: 'auto',
            height: 'auto',
            zIndex: 1,
            transform: 'translateX(-50%) translateY(-50%)',
            backgroundSize: 'cover',
            transition: '1s opacity',
          }}
        >
          <source src={webm} type="video/webm" />
          <source src={mp4} type="video/mp4" />
        </video>
      )}
      <div style={{ zIndex: 9, position: 'relative' }}>{children}</div>
    </div>
  );
};

CoverBackground.defaultProps = {
  backgroundColor: '#733791',
  minHeight: 100,
  mp4: null,
  theme: 'padding--lg align--center justify--center',
  webm: null,
};

CoverBackground.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.object.isRequired,
  minHeight: PropTypes.number,
  mp4: PropTypes.string,
  src: PropTypes.string.isRequired,
  theme: PropTypes.string,
  webm: PropTypes.string,
};

export default CoverBackground;

/**
 * @property {string} backgroundColor - The default background color behind the image or video
 * @property {object} children - Any valid JSX to show inside of the div, you need to style these on your own
 * @property {number} minHeight - A set minimum height of the div, defaults to 100, you can pass 0 to override if you want no min height
 * @property {string} mp4 - If using video, the mp4 source
 * @property {string} src - The image src if using an image
 * @property {string} theme - Classes to be applied, if none, Stiles will center it vertically and horizontally if you're using our scss
 * @property {string} webm - If using video, the webm source
 */
