import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';

const HeaderBar = ({
  backgroundColor,
  center,
  container,
  fontColor,
  fixed,
  height,
  left,
  mobileOnly,
  right,
  theme,
}) => {
  useEffect(() => {
    if (fixed === true)
      document.getElementsByTagName('body')[0].style.marginTop = `${height +
        10}px`;
  }, [fixed, height]);

  if (
    mobileOnly === true &&
    typeof window !== 'undefined' &&
    window.innerWidth > 991
  )
    return false;
  return (
    <section
      id="nwc--header-bar"
      className={theme}
      style={{
        height,
        lineHeight: `${height}px`,
        backgroundColor,
        color: fontColor,
        position: fixed === true ? 'fixed' : 'relative',
        width: fixed === true ? '100%' : null,
        top: 0,
        left: 0,
      }}
    >
      <div
        className={container}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignContent: 'center',
        }}
      >
        <div className="nwc--header-left" style={leftStyle}>
          {left}
        </div>
        <div className="nwc--header-center" style={centerStyle}>
          {center}
        </div>
        <div className="nwc--header-right" style={rightStyle}>
          {right}
        </div>
      </div>
    </section>
  );
};

const leftStyle = {
  display: 'inline-block',
  marginRight: 10,
  flex: 1,
};

const centerStyle = {
  display: 'inline-block',
  marginRight: 10,
  flex: 2,
};

const rightStyle = {
  display: 'inline-block',
  textAlign: 'right',
  flex: 1,
};

HeaderBar.defaultProps = {
  backgroundColor: '#fff',
  container: 'container--lg',
  fontColor: '#000',
  fixed: false,
  height: 56,
  mobileOnly: false,
  theme: 'shadow--sm',
};

HeaderBar.propTypes = {
  backgroundColor: PropTypes.string,
  center: PropTypes.object.isRequired,
  container: PropTypes.string,
  fontColor: PropTypes.string,
  fixed: PropTypes.bool,
  height: PropTypes.number,
  left: PropTypes.object.isRequired,
  mobileOnly: PropTypes.bool,
  right: PropTypes.object.isRequired,
  theme: PropTypes.string,
};

export default HeaderBar;

/**
 * @property {string} backgroundColor - The color of the headerbar, any hex or rgba value should work, white by defaul
 * @property {object} center - The center content, any valid JSX element
 * @property {string} container - The container class around the left, center, right sections. If using Stiles library and valid container will work: container, container--lg, etc...
 * @property {string} fontColor - The font color of the items in the header
 * @property {bool} fixed - If the header bar should stay fixed at the top while scrolling (will add margin to the body if true)
 * @property {number} height - How tall the header bar should be, defaults to 56
 * @property {object} left - The left content, any valid JSX element, most common use is with the hamburger component
 * @property {bool} mobileOnly - If true, the header will not show on desktop, else it will show for all devices
 * @property {object} right - The right content, any valid JSX element
 * @property {string} theme - Classes to be applied to the wrapper
 */
