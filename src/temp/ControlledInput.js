import React from 'react';
import { PropTypes } from 'prop-types';

const ControlledInput = ({
  autofill,
  blurCallback,
  callback,
  disabled,
  error,
  id,
  inputTheme,
  keyUpCallback,
  maxLength,
  minLength,
  optional,
  pattern,
  placeHolder,
  populatedValue,
  required,
  theme,
  title,
  type,
  value,
}) => (
  <div className="margin--bottom">
    <label
      data-test="component-input"
      className={`nwc--input ${theme}`}
      htmlFor={id}
    >
      {title && (
        <div className="nwc--label-wrap">
          <strong className="font--14">
            <span data-test="component-input-title">{title}</span>
            {required === true && (
              <sup
                data-test="component-input-required"
                className="font--error font--10"
              >
                {' *'}
              </sup>
            )}
            {optional === true && (
              <sup
                data-test="component-input-optional"
                className="font--grey font--10"
              >
                {' (optional)'}
              </sup>
            )}
          </strong>
        </div>
      )}
      <input
        data-test="component-input-input"
        autofill={autofill}
        className={`border paddingMD font16 ${inputTheme}`}
        defaultValue={populatedValue}
        disabled={disabled}
        id={id}
        maxLength={maxLength}
        minLength={minLength}
        name={id}
        onBlur={e => blurCallback(e.target.value, e)}
        onChange={e => callback(e.target.value, e)}
        onKeyUp={e => keyUpCallback(e)}
        pattern={pattern}
        placeholder={placeHolder}
        type={type}
        value={value}
      />
      {error !== null && (
        <aside
          data-test="component-input-error"
          className="nwc--validation nwc--validation--error font--12"
        >
          {error}
        </aside>
      )}
    </label>
  </div>
);

ControlledInput.defaultProps = {
  autofill: 'yes',
  blurCallback: () => true,
  disabled: false,
  error: null,
  inputTheme: '',
  keyUpCallback: () => true,
  maxLength: 50,
  minLength: 1,
  optional: false,
  pattern: null,
  placeholder: null,
  populatedValue: null,
  required: false,
  theme: '',
  type: 'text',
  value: null,
};

ControlledInput.propTypes = {
  autofill: PropTypes.string,
  blurCallback: PropTypes.func,
  callback: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  inputTheme: PropTypes.string,
  keyUpCallback: PropTypes.func,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  optional: PropTypes.bool,
  pattern: PropTypes.string,
  populatedValue: PropTypes.string,
  placeHolder: PropTypes.string,
  required: PropTypes.bool,
  theme: PropTypes.string,
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
};

export default ControlledInput;

/**
 * Super customizable inputs that you can style to ur hearts desire.
 * Callbacks for blur and change let you control logic in your parent component easily.
 *
 * @property {string} autofill - If the input should allow autofilling from the browser. This is not always handled properly in Chrome, so we suggest to use a string like 'nope'.
 * @property {func} blurCallback - The function to run when the input loses focus/blurred.
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} error - An error message to be shown docked at the bottom of the input. If null, nothing will show.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {string} inputTheme - Classes to be added to the input itself.
 * @property {func} keyUpCallback - Callback when a key is released
 * @property {number} maxLength - The allowed max length of characters into the input.
 * @property {number} minLength - The allowed min lenght of characters into the input.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {string} pattern - Regex to match on the input.
 * @property {string} placeHolder - Placeholder value to show on the input before any value is entered.
 * @property {string} populatedValue - The default value to be populated into the input on load.
 * @property {bool} required - If true, will show a required *'s. If false or not supplied, nothing will show.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} type - The type attribute to determine what sort of input is used (text, email, etc).
 * @property {string} value - The populated value on the string
 *
 */
