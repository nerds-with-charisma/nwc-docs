export const passwordValidator = (
  pw,
  length = 6,
  shouldHaveNumber = true,
  shouldHaveCapital = true,
  shouldHaveNoSpecialChars = true,
  invalidChars = '?!'
) => {
  if (!pw) return false;

  // test length
  if (pw.length >= length) {
    for (let loop = 0; loop < pw.length; loop += 1) {
      // cannot have spaces
      if (pw.charCodeAt(loop) === 32) {
        // does not, throw an error
        return {
          error: true,
          message: `Spaces are not allowed`,
        };
      }
    }
  } else {
    // does not, throw an error
    return {
      error: true,
      message: `Your password must be at least ${length} characters long`,
    };
  }

  // test if it should have a number
  if (shouldHaveNumber === true && !/\d/.test(pw)) {
    // does not, throw an error
    return {
      error: true,
      message: `Passwords must have a number`,
    };
  }

  //test if there are  any special characters
  if (shouldHaveNoSpecialChars === true) {
    for (let loop = 0; loop < pw.length; loop += 1) {
      if (invalidChars.indexOf(pw.charAt(loop)) !== -1) {
        // has invalid char, throw error
        return {
          error: true,
          message: `Password cannot contain ${invalidChars}`,
        };
      }
    }
  }

  // test if it has a capital letter
  if (shouldHaveCapital === true && !/[A-Z]/.test(pw)) {
    // does not, throw an error
    return {
      error: true,
      message: `Password must have a capital letter`,
    };
  }

  return {
    error: false,
    message: `Password is valid`,
  };
};

/**
 * @param {string} password - string value to test
 * @param {number} length - how long the password should be, defaults to 6
 * @param {boolean} shouldHaveNumber - should the password require a number, default true
 * @param {boolean} shouldHaveCapital - should a capital letter be required, in any character, default true,
 * @param {boolean} shouldHaveNoSpecialChars - should we exclude any characters, defaults to true
 * @param {string} invalidChars - if "shouldHaveNoSpecialChars" is true, which chars should we exclude
 */
