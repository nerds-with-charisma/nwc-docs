import React from 'react';

import HeroBg from '../images/hero.jpg';
import HeroImg from '../images/iphone-mockups.png';
import Logo from '../images/logo--stiles.svg';

export const statics = {
  footer: {
    copyright: `copyright © ${new Date().getFullYear()} nerdswithcharisma.com`,
    emails: (
      <div>
        <a href="mailto:briandausman@gmail.com">
          <strong className="font--primary">{'brian dausman'}</strong>
        </a>
        {' + '}
        <a href="mailto:briandausman@gmail.com">
          <strong className="font--primary">{'andrew bieganski'}</strong>
        </a>
      </div>
    ),
    tagline: 'Designed & Developed By',
  },
  header: {
    logoSrc: Logo,
    logoAlt: 'Styles Docs',
  },
  homepage: {
    hero: {
      background: HeroBg,
      tagline1: 'A set of rad styles & easy to customize components',
      tagline2: 'Stiles.',
      tagline3: 'by NWC',
      button1Text: 'Check Out the Styles',
      button1Url: '/styles',
      button2Text: 'View the Components',
      button2Url: '/components',
      src: HeroImg,
    },
    gettingStarted: {
      heading: 'Quick Start',
    },
    versionInfo: {
      heading: 'Updates',
      tagline: 'Changelog',
      updates: [
        {
          update: 'Add passwordValidator function',
          version: '3.1.15',
        },
        {
          update: 'Adjust label position for input',
          version: '3.1.12',
        },
        {
          update: 'Update layout of checkbox/radio to center vertically',
          version: '3.1.11',
        },
        {
          update: 'Wrap Input in a div to help with stacking issue',
          version: '3.1.10',
        },
        {
          update: 'Wrap Input in a div to help with stacking issue',
          version: '3.1.9',
        },
        {
          update: 'Fixed minor issue with cover background. Updated docs',
          version: '3.1.8',
        },
        {
          update: "Added a compiled css file if you aren't using sass",
          version: '3.1.7',
        },
        {
          update: 'Add key up callback to Input component',
          version: '3.1.6',
        },
        {
          update: 'Add "event" as second returned value in Input component',
          version: '3.1.5',
        },
        {
          update: 'Add simple mobile header component',
          version: '3.1.4',
        },
        {
          update: 'Type-o fix in cover component',
          version: '3.1.3',
        },
        {
          update:
            'Add full cover image/video component, added justify--content class',
          version: '3.1.2',
        },
        {
          update: 'Add tooltip component',
          version: '3.1.1',
        },
      ],
      version: '3.1.15',
    },
  },
  navigation: [
    {
      url: '/',
      text: 'GETTING STARTED',
    },
    {
      url: '/styles',
      text: 'STYLES',
    },
    {
      url: '/components',
      text: 'COMPONENTS',
    },
  ],
  styles: {
    navigation: [
      {
        text: 'Installation',
        url: 'styles',
        subs: [
          { text: 'What Is?', url: '#whatIsIt' },
          { text: 'Use It!', url: '#useIt' },
          { text: 'Reset', url: '#reset' },
        ],
      },
      {
        text: 'Colors',
        url: 'styles#colors',
        subs: [
          { text: 'Swatches', url: '#swatches' },
          { text: 'Customize', url: '#customize' },
        ],
      },
      {
        text: 'Borders',
        url: 'styles#borders',
        subs: [
          { text: 'Radii', url: '#radii' },
          { text: 'Sizing', url: '#sizing' },
          { text: 'Rules', url: '#rules' },
        ],
      },
      {
        text: 'Buttons',
        url: 'styles#buttons',
        subs: [],
      },
      {
        text: 'Layout',
        url: 'styles#layout',
        subs: [
          { text: 'Grid', url: '#grid' },
          { text: 'Container', url: '#container' },
        ],
      },
      {
        text: 'Helpers',
        url: 'styles#helpers',
        subs: [
          { text: 'Transition', url: '#transition' },
          { text: 'Padding', url: '#padding' },
          { text: 'Margins', url: '#margins' },
          { text: 'Positioning', url: '#positioning' },
          { text: 'Floats', url: '#floats' },
          { text: 'Display', url: '#display' },
          { text: 'Hiding', url: '#hiding' },
          { text: 'Opacity', url: '#opacity' },
          { text: 'Tables', url: '#tables' },
        ],
      },
      {
        text: 'Image',
        url: 'styles#images',
        subs: [],
      },
      {
        text: 'Typography',
        url: 'styles#typography',
        subs: [],
      },
      {
        text: 'Shadows',
        url: 'styles#shadows',
        subs: [],
      },
    ],
  },
  components: {
    navigation: [
      {
        text: 'Installation',
        url: 'styles',
        subs: [
          { text: 'Accordion', url: '#accordion' },
          { text: 'Alerts', url: '#alerts' },
          { text: 'Breadcrumbs', url: '#breadcrumbs' },
          { text: 'Button Group', url: '#button-group' },
          { text: 'Checkbox', url: '#checkbox' },
          { text: 'Cover Background', url: '#cover-background' },
          { text: 'Drawer', url: '#drawer' },
          { text: 'FAB', url: '#fab' },
          { text: 'Text Input', url: '#input' },
          { text: 'Modal', url: '#modal' },
          { text: 'Media Queries', url: '#mq' },
          { text: 'Mask Phone', url: '#mask-phone' },
          { text: 'Mobile Header', url: '#mobile-header' },
          { text: 'Password Validator', url: '#passwordValidator' },
          { text: 'Radio', url: '#radio' },
          { text: 'Scroll To Top', url: '#scroll-to-top' },
          { text: 'Select', url: '#select' },
          { text: 'SEO Helper', url: '#seo' },
          { text: 'State Dropdown', url: '#stateDropdown' },
          { text: 'Text Area', url: '#text-area' },
          { text: 'Tooltip', url: '#tooltip' },
          { text: 'Validate Email', url: '#validate-email' },
        ],
      },
    ],
  },
};
