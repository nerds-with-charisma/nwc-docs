import React from 'react';

const Grid = ({ xs, sm, md, lg }) => (
  <div
    className={`font--8 bg--offLight text--center col-${xs} padding--sm margin--bottom border`}
  >
    col-{xs}
    &nbsp; col-sm-{sm}
    &nbsp; col-md-{md}
    &nbsp; col-lg-{lg}
  </div>
);

export default Grid;
