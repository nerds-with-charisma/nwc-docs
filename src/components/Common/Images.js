import React from 'react';

const Images = () => (
  <div id="images">
    <div>
      <img
        className="responsive"
        src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
        alt="responsive class"
      />
    </div>

    <div style={{ width: 150 }}>
      <img
        className="responsive"
        src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
        alt="responsive class"
      />
    </div>
  </div>
);

export default Images;
