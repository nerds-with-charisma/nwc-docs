import React from 'react';

const Typography = () => (
  <>
    <h1 className="font--10">{'font--10'}</h1>
    <br />
    <h1 className="font--25">{'font--25'}</h1>
    <br />
    <h1 className="font--60">{'font--60'}</h1>

    <br />
    <br />
    <strong>{'Alignment'}</strong>
    <br />
    <div className="container">
      <div className="col-12 padding--sm margin--md bg--grey text--left font--14">
        {'text--left'}
      </div>
      <div className="col-12 padding--sm margin--md bg--grey text--right font--14">
        {'text--right'}
      </div>
      <div className="col-12 padding--sm margin--md bg--grey text--center font--14">
        {'text--center'}
      </div>

      <div
        className="col-12 padding--sm margin--md bg--grey align--center font--14"
        style={{ height: 100 }}
      >
        {'align--center'}
      </div>

      <div
        className="col-12 padding--sm margin--md bg--grey align--center justify--center font--14"
        style={{ height: 100 }}
      >
        {'justify--center & align--center'}
      </div>
    </div>
    <br />
    <br />
    <br />
  </>
);

export default Typography;
