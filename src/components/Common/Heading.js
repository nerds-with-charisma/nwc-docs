import React from 'react';
import PropTypes from 'prop-types';

const Heading = ({ theme, title }) => (
  <h2 className={`nwc--heading ${theme}`}>
    <strong>{title}</strong>
  </h2>
);

Heading.defaultProps = {
  theme: null,
};

Heading.propTypes = {
  theme: PropTypes.string,
  title: PropTypes.string.isRequired,
};

export default Heading;
