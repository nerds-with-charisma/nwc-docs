import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

const AlphaButton = ({ text, url }) => (
  <Link
    className="btn radius--lg font--16 padding--md shadow--lg block--xs block--sm"
    style={{ background: 'rgba(255,255,255,0.2)' }}
    to={url}
  >
    <strong>{text}</strong>
  </Link>
);

AlphaButton.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default AlphaButton;
