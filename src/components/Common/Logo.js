import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

const Logo = ({ alt, id, src, url }) => (
  <Link to={url}>
    <img alt={alt} id={id} src={src} />
  </Link>
);

Logo.defaultProps = {
  alt: "My website's logo",
  id: null,
  url: null,
};

Logo.propTypes = {
  alt: PropTypes.string,
  id: PropTypes.string,
  src: PropTypes.string.isRequired,
  url: PropTypes.string,
};

export default Logo;
