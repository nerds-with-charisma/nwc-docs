import React from 'react';

const Opacity = ({ theme }) => <div className={`col-4 ${theme}`}>{theme}</div>;

export default Opacity;
