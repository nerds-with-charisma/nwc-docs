import React from 'react';

import Grid from './Grid';
import Heading from './Heading';

const Grids = () => (
  <section id="layout">
    <Heading title="Grid" />
    <div id="grid" className="row container">
      <Grid xs="2" sm="2" md="2" lg="2" />
      <Grid xs="2" sm="2" md="2" lg="2" />
      <Grid xs="2" sm="2" md="2" lg="2" />
      <Grid xs="2" sm="2" md="2" lg="2" />
      <Grid xs="2" sm="2" md="2" lg="2" />
      <Grid xs="2" sm="2" md="2" lg="2" />

      <Grid xs="4" sm="4" md="4" lg="4" />
      <Grid xs="4" sm="4" md="4" lg="4" />
      <Grid xs="4" sm="4" md="4" lg="4" />

      <Grid xs="3" sm="4" md="6" lg="6" />
      <Grid xs="3" sm="4" md="6" lg="6" />
      <Grid xs="3" sm="4" md="6" lg="6" />
      <Grid xs="3" sm="4" md="6" lg="6" />

      <Grid xs="6" sm="6" md="12" lg="12" />
      <Grid xs="6" sm="6" md="12" lg="12" />
    </div>
    <br />
    <br />

    <Heading title="Container" />
    <div className="container bg--primary">
      <div className="row">
        <div className="col-12 bg--grey font--9 padding--xs text--center">
          .conainer
        </div>
      </div>
    </div>

    <div className="container--md bg--primary">
      <div className="row">
        <div className="col-12 bg--grey font--9 padding--xs text--center">
          .conainer--md
        </div>
      </div>
    </div>

    <div className="container--sm bg--primary">
      <div className="row">
        <div className="col-12 bg--grey font--9 padding--xs text--center">
          .conainer--sm
        </div>
      </div>
    </div>

    <div className="container--xs bg--primary">
      <div className="row">
        <div className="col-12 bg--grey font--9 padding--xs text--center">
          .conainer--xs
        </div>
      </div>
    </div>
  </section>
);

export default Grids;
