import React from 'react';

const RepoLinks = () => (
  <div id="#repos" className="text--right hidden--xs hidden--sm">
    <a
      href="https://bit.dev/nerdswithcharisma/stiles"
      target="_blank"
      rel="noopener noreferrer"
      className="btn font--dark no--wrap radius--lg font--14"
    >
      <strong>Bit Repo</strong>
    </a>
    &nbsp;&nbsp;&nbsp;
    <a
      href="https://www.npmjs.com/package/@nerds-with-charisma/nerds-style-sass"
      target="_blank"
      rel="noopener noreferrer"
      className="btn font--dark no--wrap radius--lg font--14"
    >
      <strong>NPM Repo</strong>
    </a>
    &nbsp;&nbsp;&nbsp;
    <a
      href="https://gitlab.com/nerds-with-charisma/nerds-style-sass"
      target="_blank"
      rel="noopener noreferrer"
      className="btn font--dark no--wrap radius--lg font--14"
    >
      <strong>GitLab Repo</strong>
    </a>
    <br />
    <br />
  </div>
);

export default RepoLinks;
