import React from 'react';
import { Link } from 'gatsby';

const NavItems = ({ currentAnchor, parent, page }) => (
  <li className="padding--bottom">
    <strong className="font--18">
      <Link to={parent.url} activeClassName="active">
        {parent.text}
      </Link>
    </strong>
    <ul>
      {parent.subs.map(sub => (
        <li className="font--12">
          <Link
            to={`/${page}${sub.url}`}
            className={sub.url === currentAnchor ? 'active' : 'font--dark'}
          >
            {sub.text}
          </Link>
        </li>
      ))}
    </ul>
  </li>
);

export default NavItems;
