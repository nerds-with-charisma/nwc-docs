import React, { useState } from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

import {
  Drawer,
  DrawerItem,
  Hamburger,
} from '@nerds-with-charisma/nerds-style-sass';

const MobileNav = ({ navigation }) => {
  const [showClose, showCloseSetter] = useState(false);

  return (
    <nav id="mobile--nav" className="font--12 text--right">
      <span className="float--right">
        <Hamburger
          callback={() => showCloseSetter(!showClose)}
          color="#000"
          showClose={showClose}
          theme="float--right"
        />
      </span>
      <Drawer
        drawerOpen={showClose}
        closeCb={() => showCloseSetter(false)}
        showClose
        preventScroll
        swipeToClose
      >
        <div className="padding--md text--left">
          {navigation.map(item => (
            <DrawerItem>
              <Link
                to={item.url}
                className="font--offDark padding--right padding--left block"
              >
                <strong>
                  &nbsp;&nbsp;&nbsp;
                  {item.text}
                  &nbsp;&nbsp;&nbsp;
                </strong>
              </Link>
            </DrawerItem>
          ))}
        </div>
      </Drawer>
    </nav>
  );
};

MobileNav.propTypes = {
  navigation: PropTypes.array.isRequired,
};

export default MobileNav;
