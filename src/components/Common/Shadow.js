import React from 'react';

const Shadow = ({ value }) => (
  <div className="col-4 col-sm-4 col-md-2 col-lg-2 text--center">
    <div
      className={`swatch bg--primary radius--lg ${value}`}
      style={{
        display: 'block',
        width: 75,
        height: 75,
        margin: '10px auto',
      }}
    />
    <strong>{value}</strong>
    <br />
    <br />
  </div>
);

export default Shadow;
