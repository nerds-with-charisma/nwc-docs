import React from 'react';

const DivHelper = ({ classes }) => (
  <div className="col-4">
    <div className={`font--10 text--center bg--primary ${classes}`}>
      <div className="bg--grey">{classes}</div>
    </div>
  </div>
);

export default DivHelper;
