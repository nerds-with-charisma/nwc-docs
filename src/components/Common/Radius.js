import React from 'react';

const Radius = ({ color, radii, value }) => (
  <div className="col-3 text--center">
    <div
      className={`swatch bg--${color} radius--${radii}`}
      style={{
        display: 'block',
        width: 75,
        height: 75,
        margin: '10px auto',
        boxShadow: '0 4px 7px rgba(0,0,0,0.1)',
      }}
    />
    <strong className="font--14">{`radius--${radii}`}</strong>
    <br />
    <small className="font--12 font--grey">
      {value}
      {'px'}
    </small>
    <br />
    <br />
  </div>
);

export default Radius;
