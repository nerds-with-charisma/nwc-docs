import React from 'react';

import Heading from '../Common/Heading';

const Markdown = ({ data }) => {
  return (
    <div className="markdown--wrapper" id={data.nodes[0].frontmatter.slug}>
      {data.nodes[0].frontmatter.bit && (
        <a
          href={data.nodes[0].frontmatter.bit}
          rel="noopener noreferrer"
          target="_blank"
          className="float--right btn bordered--xs radius--sm font--12 font--dark"
        >
          <strong>{`View ${data.nodes[0].frontmatter.title} on Bit`}</strong>
        </a>
      )}

      <Heading title={data.nodes[0].frontmatter.title} />
      <br />
      <p className="font--14">{data.nodes[0].frontmatter.description}</p>
      <br />
      <div className="row">
        <div
          className={
            data.nodes[0].frontmatter.html === '<span></span>'
              ? 'code-container col-12'
              : 'code-container col-12 col-md-6'
          }
          dangerouslySetInnerHTML={{
            __html: data.nodes[0].html,
          }}
        />

        <div
          className={
            !data.nodes[0].html
              ? 'code-container col-12'
              : 'code-container col-12 col-md-6'
          }
          dangerouslySetInnerHTML={{
            __html: data.nodes[0].frontmatter.html,
          }}
        />
      </div>
      <br />
      <br />
    </div>
  );
};

export default Markdown;
