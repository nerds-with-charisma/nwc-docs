import React from 'react';

const Button = ({ color, value }) => (
  <div className="col-3 text--center">
    <button className={`btn ${color}`}>{'Button'}</button>
    <br />
    <strong className="font--14">{color}</strong>
    <br />
    <small className="font--12 font--grey">{value}</small>
    <br />
    <br />
  </div>
);

export default Button;
