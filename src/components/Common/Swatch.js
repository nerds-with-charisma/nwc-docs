import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Swatch = ({ color, hex }) => {
  const [isRippling, isRipplingSetter] = useState(false);

  const copyColor = () => {
    const copy = document.querySelector(`.hex--${color}`);
    copy.select();
    document.execCommand('copy');

    isRipplingSetter(true);

    setTimeout(() => {
      isRipplingSetter(false);
    }, 1000);
  };

  return (
  <div className="col-4 col-sm-4 col-md-2 col-lg-2 text--center">
    <div
      className={(isRippling) ? `swatch bg--${color} radius--lg ripple` : `swatch bg--${color} radius--lg`}
      onClick={() => copyColor()}
      style={{
        display: 'block',
        width: 75,
        height: 75,
        margin: '10px auto',
        boxShadow: '0 4px 7px rgba(0,0,0,0.1)',
      }}
    />
    <strong>{color}</strong>
    <br />
    <input className={`nwc--hex hex--${color} font--12 font--grey text--center font--dark bg--light border--none`} type="text" value={hex} />
    <br />
    <br />
  </div>
)};

Swatch.propTypes = {
  color: PropTypes.string.isRequired,
  hex: PropTypes.string.isRequired,
};

export default Swatch;
