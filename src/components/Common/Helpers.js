import React, { useState } from 'react';

import DivHelper from './DivHelper';
import Opacity from './Opacity';

const Helpers = () => {
  const [hoverClass, setHoverClass] = useState(
    'font--light transition--all bg--primary text--center'
  );
  return (
    <div id="helpers" className="col-12  ">
      <strong id="transition" className="font--14">
        {'Transition / Animation'}
      </strong>

      <div
        className={hoverClass}
        onMouseEnter={() =>
          setHoverClass('font--light transition--all bg--cta text--center')
        }
        onMouseLeave={() =>
          setHoverClass('font--light transition--all bg--primary text--center')
        }
      >
        transition--all
      </div>

      <br />
      <br />
      <strong id="padding" className="font--14">
        {'Padding'}
      </strong>
      <div className="row">
        <DivHelper classes="padding--xs" />
        <DivHelper classes="padding--sm" />
        <DivHelper classes="padding--md" />
        <DivHelper classes="padding--lg" />
        <DivHelper classes="padding--top" />
        <DivHelper classes="padding--bottom" />
        <DivHelper classes="padding--left" />
        <DivHelper classes="padding--right" />
        <DivHelper classes="padding--none" />
      </div>

      <br />
      <br />
      <strong id="margins" className="font--14">
        {'Margins'}
      </strong>
      <div className="row">
        <DivHelper classes="margin--xs" />
        <DivHelper classes="margin--sm" />
        <DivHelper classes="margin--md" />
        <DivHelper classes="margin--lg" />
        <DivHelper classes="margin--top" />
        <DivHelper classes="margin--bottom" />
        <DivHelper classes="margin--left" />
        <DivHelper classes="margin--right" />
        <DivHelper classes="margin--none" />
        <div className="col-12">
          <div
            className="bg--grey margin--auto font--10 text--center"
            style={{ width: 200 }}
          >
            margin--auto
          </div>
        </div>
      </div>
      <br />
      <br />
      <strong id="positioning" className="font--14">
        {'Positioning'}
      </strong>

      <div className="col-12">
        <div className="font--10 padding--md position--relative border border--primary">
          <span
            className="position--absolute bg--primary font--light"
            style={{ top: 0, right: 0 }}
          >
            {'position--absolute'}
          </span>
          {'position--relative'}
        </div>
      </div>

      <br />
      <br />
      <strong id="floats" className="font--14">
        {'Floats'}
      </strong>

      <div className="col-12">
        <span className="float--right bg--grey font--10 padding--md">
          {'float--right'}
        </span>
        <span className="float--left bg--grey font--10 padding--md">
          {'float--left'}
        </span>

        <span className="clear bg--grey block font--10 padding--md">
          {'clear'}
        </span>
      </div>

      <br />
      <br />
      <strong id="display" className="font--14">
        {'Display'}
      </strong>

      <div className="col-12 font--12 font--light">
        <span className="bg--primary block">{'block'}</span>
      </div>

      <div className="col-12 font--12 font--light col-3">
        <span className="bg--primary block--xs">{'block--xs'}</span>
      </div>

      <div className="col-12 font--12 font--light col-3">
        <span className="bg--primary block--sm">{'block--sm'}</span>
      </div>

      <div className="col-12 font--12 font--light col-3">
        <span className="bg--primary block--md">{'block--md'}</span>
      </div>

      <div className="col-12 font--12 font--light col-3">
        <span className="bg--primary block--lg">{'block--lg'}</span>
      </div>

      <br />
      <br />
      <div
        className="bg--grey no--wrap font--12 font--dark"
        style={{ width: 40 }}
      >
        {'no--wrap'}{' '}
        {
          ' => Prevents text from going to a new line even when the parent is smaller'
        }
      </div>

      <br />
      <br />
      <strong className="font--14">{'Line Height'}</strong>

      <div className="bg--grey lh--xs font--12 text--center">{'lh--xs'}</div>
      <br />

      <div className="bg--grey lh--sm font--12 text--center">{'lh--sm'}</div>
      <br />

      <div className="bg--grey lh--md font--12 text--center">{'lh--md'}</div>
      <br />

      <div className="bg--grey lh--lg font--12 text--center">{'lh--lg'}</div>

      <br />
      <br />
      <strong id="hiding" className="font--14">
        {'Hiding'}
      </strong>

      <p className="font--14">
        To hide for all devices, use the{' '}
        <b>
          <u>hide</u>
        </b>{' '}
        class.
      </p>
      <br />

      <div className="row">
        <span className="font--12 no--wrap">
          <div
            className="hidden--xs bg--primary radius--lg"
            style={{ width: 30, height: 30, marginRight: 15 }}
          />
          {'.hidden--xs'}
        </span>

        <span className="font--12 no--wrap">
          <div
            className="hidden--sm bg--primary radius--lg"
            style={{ width: 30, height: 30, marginRight: 15 }}
          />
          {'.hidden--sm'}
        </span>

        <span className="font--12 no--wrap">
          <div
            className="hidden--md bg--primary radius--lg"
            style={{ width: 30, height: 30, marginRight: 15 }}
          />
          {'.hidden--md'}
        </span>

        <span className="font--12 no--wrap">
          <div
            className="hidden--lg bg--primary radius--lg"
            style={{ width: 30, height: 30, marginRight: 15 }}
          />
          {'.hidden--lg'}
        </span>
        <br />
        <br />
      </div>
      <br />
      <strong id="opacity" className="font--14">
        {'Opacity'}
      </strong>

      <div className="row">
        <Opacity theme="opacity--1" />
        <Opacity theme="opacity--2" />
        <Opacity theme="opacity--3" />
        <Opacity theme="opacity--4" />
        <Opacity theme="opacity--5" />
        <Opacity theme="opacity--6" />
        <Opacity theme="opacity--7" />
        <Opacity theme="opacity--8" />
        <Opacity theme="opacity--9" />
      </div>
      <br />
      <br />
      <strong id="tables" className="font--14">
        {'Tables'}
      </strong>

      <div className="row font--14">
        <table>
          <tbody>
            <tr>
              <th>Table</th>
            </tr>
            <tr>
              <td>
                {
                  'Tables are kept very simple. There\'s a reset, and some helper classes. You can use ".table-bordered" to give the table a border, or ".table-striped" to give the table alternating colored rows, and finally ".table-hover" which will make any row hovered over highlight.'
                }
              </td>
            </tr>
          </tbody>
        </table>
        <br />
        <br />
        <table className="table--bordered">
          <tbody>
            <tr>
              <th>.table--bordered</th>
            </tr>
            <tr>
              <td>{'Adds a border to all table cells.'}</td>
            </tr>
          </tbody>
        </table>

        <br />
        <br />
        <table className="table--striped">
          <tbody>
            <tr>
              <th>.table--striped</th>
            </tr>
            <tr>
              <td>{'Highlights every other row.'}</td>
            </tr>
            <tr>
              <td>{'Highlights every other row.'}</td>
            </tr>
          </tbody>
        </table>

        <br />
        <br />
        <table className="table--hover">
          <tbody>
            <tr>
              <th>.table--hover</th>
            </tr>
            <tr>
              <td>{'Highlights the row the mouse hovers over.'}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <br />
      <br />
    </div>
  );
};

export default Helpers;
