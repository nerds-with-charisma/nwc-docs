import React, { useEffect, useState } from 'react';

import Heading from './Heading';
import NavItems from './NavItems';

const DocsNav = ({ location, statics }) => {
  const [currentAnchor, currentAnchorSetter] = useState(location.hash);

  useEffect(() => {
    currentAnchorSetter(location.hash);
  }, [location]);

  return (
    <nav id="docs--nav">
      <Heading title={location.pathname.substr(1)} />
      <hr />
      <ul className="lh--sm">
        {location.pathname === '/styles'
          ? statics.styles.navigation.map(parent => (
              <NavItems
                currentAnchor={currentAnchor}
                parent={parent}
                page="styles"
              />
            ))
          : statics.components.navigation.map(parent => (
              <NavItems
                currentAnchor={currentAnchor}
                parent={parent}
                page="components"
              />
            ))}
      </ul>
    </nav>
  );
};

export default DocsNav;
