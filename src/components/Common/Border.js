import React from 'react';

const Border = ({ color, radii, value }) => (
  <div className="col-3 col-sm-3 col-md-2 col-lg-2 text--center">
    <div
      className={`swatch bg--${color} ${radii}`}
      style={{
        display: 'block',
        width: 75,
        height: 75,
        margin: '10px auto',
      }}
    />
    <strong className="font--14">{radii}</strong>
    <br />
    <small className="font--12 font--grey">
      {value}
      {'px'}
    </small>
    <br />
    <br />
  </div>
);

export default Border;
