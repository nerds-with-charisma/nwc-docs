import React from 'react';

import Border from '../Common/Border';
import Heading from '../Common/Heading';
import Radius from '../Common/Radius';

const Borders = () => (
  <div id="borders">
    <Heading title="Radii" />
    <div id="radii" className="row">
      <Radius color="primary" radii="xs" value="3" />
      <Radius color="primary" radii="sm" value="5" />
      <Radius color="primary" radii="md" value="10" />
      <Radius color="primary" radii="lg" value="300" />
    </div>
    <Heading title="Sizing & Positioning" />
    <div id="sizing" className="row">
      <Border color="offLight" radii="border--none" value="0" />
      <Border color="offLight" radii="border border--xs" value="1" />
      <Border color="offLight" radii="border border--sm" value="5" />
      <Border color="offLight" radii="border border--md" value="10" />
      <Border color="offLight" radii="border border--lg" value="25" />
      <Border color="offLight" radii="border border--top" value="1" />
      <Border color="offLight" radii="border border--right" value="1" />
      <Border color="offLight" radii="border border--bottom" value="1" />
      <Border color="offLight" radii="border border--left" value="1" />
    </div>

    <Heading title="Horizontal Rules" />
    <br id="rules" />
    <br />
    <hr />
    <div className="text--center">
      <strong className="font--14">{'<hr />'}</strong>
    </div>
    <br />
    <hr className="shorty" />
    <div className="text--center">
      <strong className="font--14">{'shorty'}</strong>
      <br />
      <small className="font--12 font--grey">{'60px'}</small>
    </div>
  </div>
);

export default Borders;
