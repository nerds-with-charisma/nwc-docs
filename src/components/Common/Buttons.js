import React from 'react';

import Button from './Button';

const Buttons = () => (
  <div id="buttons" className="row">
    <Button color="default" value="btn.default" />
    <Button color="primary" value="btn.primary" />
    <Button color="font--14 success" value="btn.font--14.success" />
    <Button color="cta round" value="cta.round" />
    <Button color="cta round block" value="cta.block" />
    <Button color="primary opacity--4" value="primary.opacity--4" />
    <Button color="success radius--sm" value="success.radius--sm" />
  </div>
);

export default Buttons;
