import React from 'react';

import Shadow from './Shadow';

const Shadows = () => (
  <section id="shadows" className="row font--14">
    <Shadow value="shadow--xs" />
    <Shadow value="shadow--sm" />
    <Shadow value="shadow--md" />
    <Shadow value="shadow--lg" />
    <Shadow value="shadow--xl" />

    <div className="col-12">
      <br />
      <h4 className="text-shadow--xs">{'.text-shadow--xs'}</h4>
      <h4 className="text-shadow--sm">{'.text-shadow--sm'}</h4>
      <h4 className="text-shadow--md">{'.text-shadow--md'}</h4>
      <h4 className="text-shadow--lg">{'.text-shadow--lg'}</h4>
    </div>
  </section>
);

export default Shadows;
