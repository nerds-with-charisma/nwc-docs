import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

const Nav = ({ navigation }) => (
  <nav id="nav" className="font--12">
    {navigation.map(item => (
      <Link
        to={item.url}
        activeClassName="font--ctaAlt"
        className="font--offDark padding--right padding--left"
      >
        <strong>
          &nbsp;&nbsp;&nbsp;
          {item.text}
          &nbsp;&nbsp;&nbsp;
        </strong>
      </Link>
    ))}
  </nav>
);

Nav.propTypes = {
  navigation: PropTypes.array.isRequired,
};

export default Nav;
