import React from 'react';

import Swatch from './Swatch';

const Colors = () => (
  <section id="swatches" className="row">
    <Swatch color="primary" hex="#9012FE" />
    <Swatch color="primaryAlt" hex="#C212FE" />
    <Swatch color="cta" hex="#F20028" />
    <Swatch color="ctaAlt" hex="#ff0084" />
    <Swatch color="links" hex="#f233b9" />
    <Swatch color="linksHover" hex="#02e2ca" />
    <Swatch color="success" hex="#85DD33" />
    <Swatch color="successAlt" hex="#d2ff52" />
    <Swatch color="info" hex="#4dd8ea" />
    <Swatch color="infoAlt" hex="#33d671" />
    <Swatch color="error" hex="#F20028" />
    <Swatch color="errorAlt" hex="#ff0084" />
    <Swatch color="text" hex="#484848" />
    <Swatch color="grey" hex="#d7d7d7" />
    <Swatch color="dark" hex="#000" />
    <Swatch color="offDark" hex="#2c2c2c" />
    <Swatch color="light" hex="#fff" />
    <Swatch color="offLight" hex="#f7f7f7" />
  </section>
);

export default Colors;