import React from 'react';
import PropTypes from 'prop-types';

const Footer = ({ statics }) => (
  <footer id="footer" className="text--center padding--lg font--14 lh--md">
    <br />
    <br />
    <strong>{statics.footer.tagline} </strong>
    <br />
    {statics.footer.emails}
    <br />
    <small>{statics.footer.copyright}</small>
  </footer>
);

Footer.propTypes = {
  statics: PropTypes.object.isRequired,
};

export default Footer;
