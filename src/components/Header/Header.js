import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { mq } from '@nerds-with-charisma/nerds-style-sass';

import Logo from '../Common/Logo';
import MobileNav from '../Common/MobileNav';
import Nav from '../Common/Nav';

const Header = ({ statics }) => {
  const [size, sizeSetter] = useState('lg');

  useEffect(() => {
    sizeSetter(mq());
  }, []);

  return (
    <header id="header" className="bg--light shadow--lg">
      <div className="container">
        <div className="row padding--top padding--bottom lh--lg">
          <section id="logo" className="col-6">
            <Logo
              alt={statics.header.logoAlt}
              id="logo"
              src={statics.header.logoSrc}
              url="/"
            />
          </section>
          <section id="nav" className="col-6 text--right">
            {size === 'md' || size === 'lg' ? (
              <Nav navigation={statics.navigation} />
            ) : (
              <MobileNav navigation={statics.navigation} />
            )}
          </section>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  statics: PropTypes.object.isRequired,
};

export default Header;
