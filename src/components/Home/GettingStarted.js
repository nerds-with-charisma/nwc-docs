import React from 'react';
import PropTypes from 'prop-types';

import GettingStartedMd from './GettingStartedMd';
import Heading from '../Common/Heading';

const GettingStarted = ({ statics }) => (
  <div className="col-12 col-md-8">
    <br />
    <Heading
      theme="text--center font--40"
      title={statics.homepage.gettingStarted.heading}
    />
    <br />
    <GettingStartedMd />
    <br />
  </div>
);

GettingStarted.propTypes = {
  statics: PropTypes.object.isRequired,
};

export default GettingStarted;
