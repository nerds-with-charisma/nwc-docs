import React from 'react';
import { graphql, Link, useStaticQuery } from 'gatsby';

const GettingStartedMd = () => {
  const data = useStaticQuery(graphql`
    {
      allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/installIt.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            title
          }
        }
      }

      useIt: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/useIt.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            title
          }
        }
      }
    }
  `);

  return (
    <div className="bg--light border--offLight border radius--sm shadow--lg">
      <div className="padding--md">
        <strong>{data.allMarkdownRemark.nodes[0].frontmatter.title}</strong>
        <div
          className="code-container"
          dangerouslySetInnerHTML={{
            __html: data.allMarkdownRemark.nodes[0].html,
          }}
        />
        <br />
        <strong>{data.useIt.nodes[0].frontmatter.title}</strong>
        <div
          className="code-container"
          dangerouslySetInnerHTML={{
            __html: data.useIt.nodes[0].html,
          }}
        />
      </div>
      <footer className="bg--offLight border--top padding--md text--right">
        <Link
          to="/styles"
          className="btn radius--lg border--none font--light shadow--lg  bg--ctaAlt font--14 gradient"
        >
          <strong>
            &nbsp;&nbsp;&nbsp;{'Start Exploring'}&nbsp;&nbsp;&nbsp;
          </strong>
        </Link>
      </footer>
    </div>
  );
};

export default GettingStartedMd;
