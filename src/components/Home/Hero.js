import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { CoverBackground, mq } from '@nerds-with-charisma/nerds-style-sass';

import AlphaButton from '../Common/AlphaButton';

const Hero = ({ statics }) => {
  const [m] = useState(mq());
  const [w, wSetter] = useState(null);

  useEffect(() => {
    if (m === 'xl') wSetter('47%');
    if (m === 'lg') wSetter('62%');
    if (m === 'md') wSetter('58%');
  }, [m]);

  return (
    <section id="hero" className="overflow--hidden text-shadow--md">
      <CoverBackground src={statics.homepage.hero.background} minHeight={600}>
        <div className="align--center justify--centerc container">
          <div className="col-12">
            <div className="col-12 col-md-8 font--light text--center">
              <h5 className="opacity--8 font--21 lh--sm">
                {statics.homepage.hero.tagline1}
              </h5>
              <h1 className="hidden--xs hidden--sm font--200">
                <strong>{statics.homepage.hero.tagline2}</strong>
              </h1>
              <h1 className="hidden--md hidden--lg font--90">
                <strong>{statics.homepage.hero.tagline2}</strong>
              </h1>
              <div
                className="font--21 hidden--xs hidden--sm"
                style={{ marginTop: -65 }}
              >
                {statics.homepage.hero.tagline3}
              </div>
              <br />
              <br />
              <AlphaButton
                text={statics.homepage.hero.button1Text}
                url={statics.homepage.hero.button1Url}
              />
              <br className="hidden--md hidden--lg" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <br className="hidden--md hidden--lg" />
              <AlphaButton
                text={statics.homepage.hero.button2Text}
                url={statics.homepage.hero.button2Url}
              />
            </div>
          </div>
        </div>
      </CoverBackground>
      <img
        alt="Hero Banner"
        className="hidden--xs hidden--sm"
        src={statics.homepage.hero.src}
        style={{
          position: 'absolute',
          top: 70,
          left: '47%',
          width: w,
        }}
      />
    </section>
  );
};

Hero.propTypes = {
  statics: PropTypes.object.isRequired,
};

export default Hero;
