import React from 'react';
import PropTypes from 'prop-types';

import Heading from '../Common/Heading';

const VersionInfo = ({ statics }) => (
  <section id="version--info" className="col-12 col-md-4 position--relative">
    <br />
    <Heading
      theme="text--center font--40"
      title={statics.homepage.versionInfo.heading}
    />
    <br />
    <div
      className="gradient radius--md shadow--lg font--light"
      style={{ height: 295, overflowY: 'scroll' }}
    >
      <h3 className="font--72">
        <strong>{`v${statics.homepage.versionInfo.version}`}</strong>
      </h3>
      <strong className="font--28">
        {statics.homepage.versionInfo.tagline}
      </strong>
      <hr className="opacity--3 border--light" />
      {statics.homepage.versionInfo.updates.map(update => (
        <li key={update.version} className="font--14 padding--sm">
          {`v${update.version} :: ${update.update}`}
        </li>
      ))}
      <br />
      <br />
    </div>
  </section>
);

VersionInfo.propTypes = {
  statics: PropTypes.object.isRequired,
};

export default VersionInfo;
