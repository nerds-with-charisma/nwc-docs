import React from 'react';

import { statics } from '../../utils/statics';

import '../../styles/local.scss';

import DocsNav from '../Common/DocsNav';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';

const DocsLayout = ({ children, location }) => (
  <section id="docsLayout">
    <Header statics={statics} />
    <section id="docs--wrapper" className="bg--offLight padding--lg">
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-3">
            <DocsNav location={location} statics={statics} />
          </div>
          <div className="col-12 col-md-9 bg--light padding--md radius--md shadow--lg">
            {children}
          </div>
        </div>
      </div>
    </section>
    <Footer statics={statics} />
  </section>
);

export default DocsLayout;
