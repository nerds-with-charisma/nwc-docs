---
title: 'Drawer'
slug: '#drawer'
description: "A slide out drawer perfect for a menu."
bit: 'https://bit.dev/nerdswithcharisma/stiles/drawer'
html: <span></span>
---

```javascript
import { Drawer, DrawerItem } from '@nerds-with-charisma/nerds-style-sass';
...
const [drawerOpen, drawerOpenSetter] = useState(false);

...
<div>
  <button
    type="button"
    className="btn default"
    onClick={() => drawerOpenSetter(!drawerOpen)}
  >
    Open Drawer
  </button>
  <br />
  <Drawer
    drawerOpen={drawerOpen}
    closeCb={() => drawerOpenSetter(false)}
    showClose
    preventScroll
    swipeToClose
  >
    <div className="padding--md">
      <DrawerItem>
        <a href="/stiles" className="font--dark block">
          <strong>
            {'Some Link'}
          </strong>
        </a>
      </DrawerItem>
    </div>
  </Drawer>
</div>
```
