---
title: 'FAB'
slug: '#fab'
description: "A Floating Action Button that will be docked at the bottom left or right that will have callbacks for click, mouse in, mouse leave, and is toggled by a variable \"fabOpen\". This one is docked at the bottom left of your screen."
bit: 'https://bit.dev/nerdswithcharisma/stiles/fab'
html: <span></span>
---

```javascript
import { Fab, FabItem } from '@nerds-with-charisma/nerds-style-sass';
...
const [fabOpen, fabOpenSetter] = useState(false);
...
<Fab
  left
  isActive
  fabOpen={fabOpen}
  clickCb={() => fabOpenSetter(!fabOpen)}
>
  <>
    <FabItem isImage caption="Maeby">
      <a href="/stiles">
        <img src="https://nerdswithcharisma.com/portfolio/maeby.jpg" />
      </a>
    </FabItem>
    <FabItem isImage>
      <img src="https://nerdswithcharisma.com/portfolio/maeby.jpg" />
    </FabItem>
    <FabItem backgroundColor="#ff0000" textColor="#000">
      <span>&hearts;</span>
    </FabItem>
    <FabItem>
      <span onClick={() => console.log('clicked')}>
        <span>&hearts; Test</span>
      </span>
    </FabItem>
  </>
</Fab>
```
