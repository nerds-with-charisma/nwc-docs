---
title: 'Media Queries'
slug: '#mq'
description: "A function that returns the device size in a human readable format based on the Bootstrap device breakdowns."
bit: 'https://bit.dev/nerdswithcharisma/stiles/media-queries'
html: <span></span>
---

```javascript
import { mq } from '@nerds-with-charisma/nerds-style-sass';

...
// outputs "Your screen size is: lg"
{`Your screen size is: ${mq()}`}
```


