---
title: 'Colors'
slug: '#colors'
description: "All color options we can use in helper classes"
html: <span></span>
---

```scss
class="font--{color}"
class="bg--{color}"
class="border--{color}"
```


