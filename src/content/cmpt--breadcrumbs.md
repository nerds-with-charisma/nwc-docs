---
title: 'Breadcrumbs'
slug: '#breadcrumbs'
description: "A breadcrumb that ouputs schema markup along with the visual aspect of the crumbs."
bit: 'https://bit.dev/nerdswithcharisma/stiles/breadcrumb'
html: <span></span>
---

```javascript
import { Breadcrumbs } from '@nerds-with-charisma/nerds-style-sass';

...

<Breadcrumbs
  crumbs={[
    {
      title: 'Home',
      url: '/',
      active: false,
    },
    {
      title: 'About',
      url: '/about',
      active: true,
    },
  ]}
  delimiter={'/'}
  delimiterTheme={'test-delimiter'}
  id={'test'}
  linkClass={'link-class'}
  schema={true}
  theme={'test-class'}
/>
```


