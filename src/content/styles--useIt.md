---
title: 'Use It!'
slug: 'useIt'
description: "Install via npm. Then import the main scss file into your styles file. Then use the helper classes or components listed below. OOOOOOR...if you're not using SASS, we've added a compiled CSS file as of version 3.1.7."
html: <span></span>
---

```shell
npm i @nerds-with-charisma/nerds-style-sass --save
```

```css
@import "~@nerds-with-charisma/nerds-style-sass/main.scss";
...
@import "~@nerds-with-charisma/nerds-style-sass/main.css";
```
