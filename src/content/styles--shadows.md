---
title: 'Shadows'
slug: '#shadows'
description: "For adding shadows to either text or a box shadow to an element."
html: <span></span>
---

```css
  .text-shadow--[size]
  .shadow--[size]
```