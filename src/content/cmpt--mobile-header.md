---
title: 'Mobile Header'
slug: '#mobile-header'
description: "A simple header for mobile devices, we're still working on expanding this component to be more flexible. Included is an animated Hamburger icon, that you can use as it's own component if you want to roll your own menu."
bit: 'https://bit.dev/nerdswithcharisma/stiles/header-bar'
html: <span></span>
---

```javascript
import { HeaderBar } from '@nerds-with-charisma/nerds-style-sass';
...
const [showClose, showCloseSetter] = useState(false);
...
<HeaderBar
  fixed
  center={
    <div className="text--center">
      <strong>{'Home'}</strong>
    </div>
  }
  left={
    <span>
      <Hamburger
        callback={() => showCloseSetter(!showClose)}
        color="#000"
        showClose={showClose}
      />
    </span>
  }
  right={<span>Search</span>}
/>
```

```javascript
// if you want to use the hamburger by itself
import { Hamburger } from '@nerds-with-charisma/nerds-style-sass';
...
const [showClose, showCloseSetter] = useState(false);
...
<Hamburger
  callback={() => showCloseSetter(!showClose)}
  color="#ff0000"
  height={40}
  showClose={showClose}
/>
```


