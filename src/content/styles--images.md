---
title: 'Images'
slug: '#images'
description: "Image display helpers. For responsive images that take the max width of their parent, use the 'responsive' class. You still need to compress your images properly...don't make slow websites."
html: <span></span>
---

