---
title: 'Customize'
slug: '#customize'
description: "Don't particularly care for our styles or want to customize it to your project? No problem, friend. In the same file where you import our sass file, you can paste this handy dandy mixin snippet into your file, just below the import above, and add your own colors. It will override ours and you're all good to keep rocking with your colors."
html: <span></span>
---

```scss
/* Put all your colors here */
$primary: #ff0000;
$text: purple;

/* Add the name above to these two lines */
$color_names: primary, text;
$color_vars: $primary, $text;

/* This will loop through and give you the correct helpers */
@each $name in $color_names {
  $i: index($color_names, $name);
  .bg--#{$name} { background: nth($color_vars, $i); }
  .font--#{$name} { color: nth($color_vars, $i); }
  .border--#{$name} { border-color: nth($color_vars, $i); }
}
```


