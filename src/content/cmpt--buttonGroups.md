---
title: 'Button Groups'
slug: '#button-groups'
description: "Connected buttons with similar functionality to radio buttons, but look nicer."
bit: 'https://bit.dev/nerdswithcharisma/stiles/button-group'
html: <span></span>
---

```javascript
import { ButtonGroup } from '@nerds-with-charisma/nerds-style-sass';

...

<ButtonGroup
  title="Title"
  theme="some classes"
  callback={i => console.log(i)}
  options={[
    {
      title: 'Option 1',
      value: 'option-1',
      active: true,
    },
    {
      title: 'Option 2',
      value: 'option-2',
      active: false,
    },
  ]}
/>
```


