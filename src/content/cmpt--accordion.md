---
title: 'Accordion'
slug: '#accordion'
description: "A collapse accordion that can be highly styled and customized. The multi-open option allows for more than one panel to be opened at one time or collapse all but one panel."
bit: 'https://bit.dev/nerdswithcharisma/stiles/accordion'
html: <span></span>
---

```javascript
import { Accordion } from '@nerds-with-charisma/nerds-style-sass';

...

<Accordion
  delimiter="+"
  delimiterClose="-"
  id="accordion1"
  multiOpen={false}
  options={[
    { title: 'Title 1', content: 'hello', isOpen: true },
    { title: 'Title 2', content: 'bye', isOpen: false },
  ]}
/>
```


