---
title: 'Input'
slug: '#input'
description: "A text input with several options available including an no change callback and a blur callback."
bit: 'https://bit.dev/nerdswithcharisma/stiles/input'
html: <span></span>
---

```javascript
import { Input } from '@nerds-with-charisma/nerds-style-sass';

...

<Input
  autofill
  blurCallback={v => console.log(v)}
  callback={v => console.log(v)}
  disabled={false}
  error={null}
  id="myId"
  inputTheme="test-class"
  maxLength={10}
  minLength={10}
  optional={false}
  placeHolder="Type something!"
  require
  theme="test-class-label"
  title="First Name"
  type="text"
/>
```

```javascript
import { ControlledInput } from '@nerds-with-charisma/nerds-style-sass';

...

const [someVar, someVarSetter] = useState('Hello there');

...

<Input
  callback={v => someVarSetter(v)}
  placeHolder="Type something!"
  title="First Name"
  type="text"
  value={someVar}
/>
```


