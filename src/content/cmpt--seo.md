---
title: 'SEO Helpers'
slug: '#seo-helpers'
description: "Using React Helmet, this helper component will allow you to quickly and easily add meta data, opengraph, schema markup, and more to your components in a reusable way."
bit: 'https://bit.dev/nerdswithcharisma/stiles/seo'
html: <span></span>
---

```javascript
import { Seo } from '@nerds-with-charisma/nerds-style-sass';

...
<Seo
  title="Tully : Comicbook event reading order & tracker."
  meta={[
    {
      name: 'description',
      content:
        "My description",
    },
    {
      name: 'keywords',
      content:
        'Some, keywords, here',
    },
  ]}
  openGraph={{
    description:
      'My description',
    image: 'https://nerdswithcharisma.com/nwc-tile.jpg',
    site_name: 'Nerds With Charisma',
    title: 'Nerds With Charisma',
    type: 'website',
    url: 'https://nerdswithcharisma.com',
  }}
  schema={[
    {
      '@type': 'Person',
      colleague: [],
      image: 'https://nerdswithcharisma.com/images/portfolio/brian.jpg',
      jobTitle: 'Developer',
      alumniOf: 'Northern Illinois University',
      gender: 'male',
      '@context': 'http://schema.org',
      email: 'briandausman@gmail.com',
      name: 'Brian Dausman',
      url: 'https://nerdswithcharisma.com',
    },
    ...
  ]}
/>
```


