---
title: 'Mask Phone'
slug: '#mask-phone'
description: "A function to mask a phone number to (555) 555-5555 format."
bit: 'https://bit.dev/nerdswithcharisma/stiles/mask-phone'
html: <span></span>
---

```javascript
import { maskPhone } from '@nerds-with-charisma/nerds-style-sass';

...
// outputs (555) 555-5555
console.log(maskPhone('5555555555'));
```


