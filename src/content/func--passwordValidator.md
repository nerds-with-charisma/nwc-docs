---
title: 'Validate Password'
slug: '#password-validator'
description: "A function that returns weather a password is valid or not via an error node along with a message node if an error is found. Customizable to check for length, number, capital, and special chars"
bit: null
html: <span></span>
---

```javascript
import { passwordValidator } from '@nerds-with-charisma/nerds-style-sass';

...
// (password to test, length, shouldHaveNumber, shouldHaveCapital, shouldHaveNoSpecialChars, invalidChars)

// returns { "error": false,"message": "Password is valid" }
console.log(passwordValidator('hEllo123', 4, true, true, true, '@!?'));

// returns { "error": true,"message": "Password cannot contain @!?" }
console.log(passwordValidator('hEll@', 4, true, true, true, '@!?'));
```


