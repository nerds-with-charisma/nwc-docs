---
title: 'What Is It!'
slug: 'whatIsIt'
description: 'A lightweight, modern Style and React Component framework that focuses on re-usability over anything else. Think SMACCS meets bootstrap. Fully commented and easy to customize.'
html: null
---
