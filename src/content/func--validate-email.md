---
title: 'Validate Email'
slug: '#validate-email'
description: "A function that returns weather an email is valid or not via an error node along with a message node if an error is found."
bit: 'https://bit.dev/nerdswithcharisma/stiles/validate-email'
html: <span></span>
---

```javascript
import { maskPhone } from '@nerds-with-charisma/nerds-style-sass';

...
// returns { "error": false,"message": "success" }
console.log(validateEmail('test@test.com'));

// returns { "error": true,"message": "The e-mail address you have entered is not valid. Email should be of the form abc@de.com" }
console.log(validateEmail('testtest.com')
```


