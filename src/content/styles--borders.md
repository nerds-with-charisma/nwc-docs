---
title: 'Borders'
slug: '#borders'
description: "All border styling options"
html: <span></span>
---

```scss
radius--[size]
border--[size/position]
```


