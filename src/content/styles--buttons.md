---
title: 'Buttons'
slug: '#cuttons'
description: "Button styling is minimal but highly customizable. Mix and match classes to make the perfect buttons for your project. Make a button component with your styles for easy duplication. Use other helper classes to make your ideal buttons. Combine bg--[color] with font--[color] and font--[size] to make specific buttons to fit your styles."
html: <span></span>
---


