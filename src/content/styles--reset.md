---
title: 'Reset'
slug: 'reset'
description: "We've included a very simple reset which you can check out the specifics of, at the link below."
html: <a href="https://www.digitalocean.com/community/tutorials/css-minimal-css-reset" target="_blank" class="btn font--dark no--wrap radius--lg font--14">Alligator.io Simple Reset</a>
---
