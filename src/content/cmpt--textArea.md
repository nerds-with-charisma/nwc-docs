---
title: 'Text Area'
slug: '#text-area'
description: "Similar to the Input component, but for a text area input."
bit: 'https://bit.dev/nerdswithcharisma/stiles/text-area'
html: <span></span>
---

```javascript
import { TextArea } from '@nerds-with-charisma/nerds-style-sass';

...
<TextArea
  id="test"
  blurCallback={v => console.log(v)}
  callback={() => true}
  rows="1"
  title="test"
/>
```


