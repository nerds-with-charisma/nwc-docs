---
title: 'Select Input'
slug: '#select'
description: "A prettier select input or dropdown."
bit: 'https://bit.dev/nerdswithcharisma/stiles/select'
html: <span></span>
---

```javascript
import { Select } from '@nerds-with-charisma/nerds-style-sass';

...
<Select
  callback={v => console.log(v)}
  fullWidth
  id="select--someId"
  title="Some Title"
  options={[
    {
      title: '-- Choose an Option --',
      value: '0',
    },
    {
      title: 'Option 1',
      value: '1',
    },
  ]}
/>
```


