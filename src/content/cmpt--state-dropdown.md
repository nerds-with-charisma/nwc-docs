---
title: 'State Dropdown'
slug: '#stateDropdown'
description: "A dropdown with all the states and provinces in the U.S. with a callback to send you the selected value. Also, the option to show the abbreviation or full state name."
bit: 'https://bit.dev/nerdswithcharisma/stiles/seo'
html: <span></span>
---

```javascript
import { StateDropdown } from '@nerds-with-charisma/nerds-style-sass';

...
const [stateValue, stateValueSetter] = useState(null);

...
<StateDropdown
  callback={v => stateValueSetter(v)}
  fullWidth
  id="select--someId"
  title="Some Title"
  optional
  populatedValue={stateValue}
  showFullName
/>
```


