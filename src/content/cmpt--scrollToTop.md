---
title: 'Scroll To Top'
slug: '#scroll-to-top'
description: "A fixed button that, when clicked, will scroll the page back to the top. Hint: it's over there at the bottom right, it says \"Up\"."
bit: 'https://bit.dev/nerdswithcharisma/stiles/scroll-to-top'
html: <span></span>
---

```javascript
import { ScrollToTop } from '@nerds-with-charisma/nerds-style-sass';

...
<ScrollToTop
  delimiter={<span>Up</span>}
  delimiterTheme="font--dark"
  size={50}
  theme="radius--lg shadow--md"
  threshold={100}
/>
```


