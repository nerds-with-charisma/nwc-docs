---
title: 'Tooltip'
slug: '#tooltip'
description: "A hover tooltip with customizable width, which could be calculated dynamically or sent as a static number"
bit: 'https://bit.dev/nerdswithcharisma/stiles/tooltip'
html: <span></span>
---

```javascript
import { Tooltip } from '@nerds-with-charisma/nerds-style-sass';

...
<Tooltip
  callback={() => console.log('callback')}
  content={
    <strong>
      {"This is all stuff and it's super duper cool. "}
      <br />
      <br />
      {
        'You can customize how it looks with classes, or just leave it as is and it will default to this style with our sass styles installed'
      }
    </strong>
  }
  id="myId"
  theme="font--dark"
  title="Hello"
  width={300}
/>
```


