---
title: 'Cover Background'
slug: '#cover-background'
description: "A div that has a supplied image taking up the entire background. You have the ability to set a minimum height or pass a video (prop mp4) file. It will render all children inside the div, that you can style any way you'd like."
bit: 'https://bit.dev/nerdswithcharisma/stiles/cover-background'
html: <span></span>
---

```javascript
import { CoverBackground } from '@nerds-with-charisma/nerds-style-sass';

...

<CoverBackground
  src="https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp"
  minHeight={300}
>
  <div>
    <h1 className="font--light text--center">Hi</h1>
  </div>
</CoverBackground>
```


