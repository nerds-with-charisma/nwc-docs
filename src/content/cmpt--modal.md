---
title: 'Modal'
slug: '#modal'
description: "A modal lightbox that will be visible based on a prop (show) that you pass it."
bit: 'https://bit.dev/nerdswithcharisma/stiles/modal'
html: <span></span>
---

```javascript
import { Modal } from '@nerds-with-charisma/nerds-style-sass';

...
// create a state variable to toggle the modal
const [showModal, showModalSetter] = useState(false);
...
// make a trigger somewhere to toggle state
<span onClick={() => showModalSetter(true)}>{'Show Modal'}</span>
...

<Modal
  closeCb={() => showModalSetter(false)}
  maskColor="rgba(50,20,100,0.7)"
  show={showModal}
  width={50}
  title="Imma Modal"
>
  <div className="font--14">
    <h1>The plans you refer to will soon be back in our hands.</h1>
    <p>
      Leave that to me. Send a distress signal, and inform the Senate
      that all on board were killed. Remember, a Jedi can feel the Force
      flowing through him. You're all clear, kid. Let's blow this thing
      and go home!
    </p>
    <p>
      Don't be too proud of this technological terror you've
      constructed. The ability to destroy a planet is insignificant next
      to the power of the Force. The Force is strong with this one. I
      have you now. Kid, I've flown from one side of this galaxy to the
      other. I've seen a lot of strange stuff, but I've never seen
      anything to make me believe there's one all-powerful Force
      controlling everything. There's no mystical energy field that
      controls my destiny. It's all a lot of simple tricks and nonsense.
    </p>
  </div>
</Modal>
```


