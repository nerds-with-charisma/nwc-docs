---
title: 'Checkbox'
slug: '#checkbox'
description: "A common checkbox component with a callback that passes the value and the event itself back."
bit: 'https://bit.dev/nerdswithcharisma/stiles/checkbox'
html: <span></span>
---

```javascript
import { Checkbox } from '@nerds-with-charisma/nerds-style-sass';

...

<Checkbox
  title="Check Me"
  id="input--someId"
  callback={(v, e) => {
    console.log(v);
    console.log(e);
  }}
  value="some-value"
  theme="custom styles"
/>
```


