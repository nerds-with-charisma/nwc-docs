---
title: 'Typography'
slug: '#typography'
description: "Font sizing can be applied from 1 to 200 with the \"font--[size]\" classes."
html: <span></span>
---

```css
.font--[size]
```