---
title: 'Alerts'
slug: '#alerts'
description: "A simple notification alert system that takes pretty much any child content you can throw at it."
bit: 'https://bit.dev/nerdswithcharisma/stiles/accordion'
html: <span></span>
---

```shell
theme prop: none, error, success, info, or pass your own set of classes
```

```javascript
import { AlertHeading } from '@nerds-with-charisma/nerds-style-sass';

...

<AlertHeading theme="info">
  <strong>Info</strong>
</AlertHeading>
```


