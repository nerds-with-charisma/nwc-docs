---
title: 'Radio'
slug: '#radio'
description: "Similar to the checkbox, only a radio component. You should wrap it in your own fieldset tags and supply a legend."
bit: 'https://bit.dev/nerdswithcharisma/stiles/radio'
html: <span></span>
---

```javascript
import { Radio } from '@nerds-with-charisma/nerds-style-sass';

...

<fieldset className="border--none">
  <legend>Make a choice</legend>
  <Radio
  title="Check Me"
  name="radios"
  callback={(v, e) => {
    console.log(v);
    console.log(e);
  }}
  value="some-value"
  theme="custom styles"
/>
&nbsp; &nbsp;
<Radio
  title="Or Me"
  name="radios"
  callback={(v, e) => {
    console.log(v);
    console.log(e);
  }}
  value="some-value"
  theme="custom styles"
/>
</fieldset>
```


