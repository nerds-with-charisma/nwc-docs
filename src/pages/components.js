import React, { useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';

import { getCurrentId } from '../utils/scrollHelpers';

import {
  Accordion,
  AlertHeading,
  Breadcrumbs,
  ButtonGroup,
  Checkbox,
  CoverBackground,
  Drawer,
  DrawerItem,
  Fab,
  FabItem,
  Input,
  Modal,
  mq,
  passwordValidator,
  Radio,
  ScrollToTop,
  Select,
  StateDropdown,
  TextArea,
  Tooltip,
} from '@nerds-with-charisma/nerds-style-sass';

import DocsLayout from '../components/Layouts/DocsLayout';
import Markdown from '../components/Common/Markdown';
import RepoLinks from '../components/Common/RepoLinks';

const Components = ({ location }) => {
  const data = useStaticQuery(graphql`
    {
      accordion: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--accordion.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      alerts: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--alerts.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      breadcrumbs: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--breadcrumbs.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      buttonGroups: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--buttonGroups.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      checkbox: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--checkbox.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      coverBackground: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--coverBackground.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      drawer: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--drawer.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      fab: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--fab.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      input: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--input.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      mq: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/func--mq.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      maskPhone: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/func--mask-phone.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      mobileHeader: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--mobile-header.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      modal: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--modal.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      passwordValidator: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/func--passwordValidator.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      radio: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--radio.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      scrollToTop: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--scrollToTop.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      select: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--select.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      seo: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--seo.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      stateDropdown: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--state-dropdown.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      textArea: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--textArea.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      tooltip: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/cmpt--tooltip.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }

      validateEmail: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/func--validate-email.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            bit
            description
            html
            slug
            title
          }
        }
      }
    }
  `);

  getCurrentId();

  const [drawerOpen, drawerOpenSetter] = useState(false);
  const [fabOpen, fabOpenSetter] = useState(false);
  const [showModal, showModalSetter] = useState(false);
  const [stateValue, stateValueSetter] = useState(null);

  return (
    <DocsLayout location={location}>
      <RepoLinks />
      <Markdown data={data.accordion} />
      <Accordion
        delimiter="+"
        delimiterClose="-"
        id="accordion1"
        multiOpen
        options={[
          { title: 'test', content: 'hello', isOpen: true },
          { title: 'test2', content: 'bye', isOpen: false },
        ]}
      />
      <hr id="alerts" className="padding--md" />
      <Markdown data={data.alerts} />
      <AlertHeading>
        <strong>Default</strong>
      </AlertHeading>
      <AlertHeading theme="error">
        <strong>Error</strong>
      </AlertHeading>
      <AlertHeading theme="success">
        <strong>Success</strong>
      </AlertHeading>
      <AlertHeading theme="info">
        <strong>Info</strong>
      </AlertHeading>
      <hr id="breadcrumbs" className="padding--md" />
      <Markdown data={data.breadcrumbs} />
      <Breadcrumbs
        crumbs={[
          {
            title: 'Home',
            url: '/',
            active: false,
          },
          {
            title: 'About',
            url: '/about',
            active: true,
          },
        ]}
        delimiter={'/'}
        delimiterTheme={'test-delimiter'}
        id={'test'}
        linkClass={'link-class'}
        schema={true}
        theme={'test-class'}
      />
      <hr id="button-group" className="padding--md" />
      <Markdown data={data.buttonGroups} />
      <ButtonGroup
        title="Title"
        theme="some classes"
        callback={i => console.log(i)}
        options={[
          {
            title: 'Option 1',
            value: 'option-1',
            active: true,
          },
          {
            title: 'Option 2',
            value: 'option-2',
            active: false,
          },
        ]}
      />
      <hr id="checkbox" className="padding--md" />
      <Markdown data={data.checkbox} />
      <Checkbox
        title="Check Me"
        id="input--someId"
        callback={(v, e) => {
          console.log(v);
          console.log(e);
        }}
        value="some-value"
        theme="custom styles"
      />
      <hr id="cover-background" className="padding--md" />
      <Markdown data={data.coverBackground} />
      <CoverBackground
        src="https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp"
        minHeight={300}
      >
        <div>
          <h1 className="font--light text--center">Hi</h1>
        </div>
      </CoverBackground>
      <hr id="drawer" className="padding--md" />
      <Markdown data={data.drawer} />
      <div>
        <button
          type="button"
          className="btn default"
          onClick={() => drawerOpenSetter(!drawerOpen)}
        >
          Open Drawer
        </button>
        <br />
        <Drawer
          drawerOpen={drawerOpen}
          closeCb={() => drawerOpenSetter(false)}
          showClose
          preventScroll
          swipeToClose
        >
          <div className="padding--md">
            <DrawerItem>
              <a href="/stiles" className="font--dark block">
                <strong>{'Some Link'}</strong>
              </a>
            </DrawerItem>
          </div>
        </Drawer>
      </div>
      <hr id="fab" className="padding--md" />
      <Markdown data={data.fab} />
      <Fab
        isActive
        left
        fabOpen={fabOpen}
        clickCb={() => fabOpenSetter(!fabOpen)}
      >
        <>
          <FabItem isImage caption="Maeby">
            <a href="/stiles">
              <img
                src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
                alt="Maeby"
              />
            </a>
          </FabItem>
          <FabItem isImage>
            <img
              src="https://nerdswithcharisma.com/portfolio/maeby.jpg"
              alt="Another Maeby"
            />
          </FabItem>
          <FabItem backgroundColor="#ff0000" textColor="#000">
            <span>&hearts;</span>
          </FabItem>
          <FabItem>
            <span onClick={() => console.log('clicked')}>
              <span>&hearts; Test</span>
            </span>
          </FabItem>
        </>
      </Fab>
      <hr id="input" className="padding--md" />
      <Markdown data={data.input} />
      <Input
        autofill
        blurCallback={v => console.log(v)}
        callback={v => console.log(v)}
        disabled={false}
        error={null}
        id="myId"
        inputTheme="test-class"
        maxLength={10}
        minLength={10}
        optional={false}
        placeHolder="Type something!"
        require
        theme="test-class-label"
        title="First Name"
        type="text"
      />
      <hr id="mq" className="padding--md" />
      <Markdown data={data.mq} />
      {`Your screen size is: ${mq()}`}
      <hr id="mask-phone" className="padding--md" />
      <Markdown data={data.maskPhone} />
      <hr id="mobile-header" className="padding--md" />
      <Markdown data={data.mobileHeader} />
      <hr id="modal" className="padding--md" />
      <Markdown data={data.modal} />
      <span onClick={() => showModalSetter(true)}>{'Show Modal'}</span>
      <Modal
        closeCb={() => showModalSetter(false)}
        maskColor="rgba(50,20,100,0.7)"
        show={showModal}
        width={50}
        title="Imma Modal"
      >
        <div className="font--14">
          <h1>The plans you refer to will soon be back in our hands.</h1>
          <p>
            Leave that to me. Send a distress signal, and inform the Senate that
            all on board were killed. Remember, a Jedi can feel the Force
            flowing through him. You're all clear, kid. Let's blow this thing
            and go home!
          </p>
          <p>
            Don't be too proud of this technological terror you've constructed.
            The ability to destroy a planet is insignificant next to the power
            of the Force. The Force is strong with this one. I have you now.
            Kid, I've flown from one side of this galaxy to the other. I've seen
            a lot of strange stuff, but I've never seen anything to make me
            believe there's one all-powerful Force controlling everything.
            There's no mystical energy field that controls my destiny. It's all
            a lot of simple tricks and nonsense.
          </p>
        </div>
      </Modal>

      <hr id="passwordValidator" className="padding--md" />
      <Markdown data={data.passwordValidator} />
      {JSON.stringify(
        passwordValidator('hEllo123', 4, true, true, true, '@!?')
      )}

      <hr id="radio" className="padding--md" />
      <Markdown data={data.radio} />
      <fieldset className="border--none">
        <legend>Make a choice</legend>
        <Radio
          title="Check Me"
          name="radios"
          callback={(v, e) => {
            console.log(v);
            console.log(e);
          }}
          value="some-value"
          theme="custom styles"
        />
        &nbsp; &nbsp;
        <Radio
          title="Or Me"
          name="radios"
          callback={(v, e) => {
            console.log(v);
            console.log(e);
          }}
          value="some-value"
          theme="custom styles"
        />
      </fieldset>
      <hr id="scroll-to-top" className="padding--md" />
      <Markdown data={data.scrollToTop} />
      <ScrollToTop
        delimiter={<span>Up</span>}
        delimiterTheme="font--dark"
        size={50}
        theme="radius--lg shadow--md"
        threshold={100}
      />
      <hr id="select" className="padding--md" />
      <Markdown data={data.select} />
      <Select
        callback={v => console.log(v)}
        fullWidth
        id="select--someId"
        title="Some Title"
        options={[
          {
            title: '-- Choose an Option --',
            value: '0',
          },
          {
            title: 'Option 1',
            value: '1',
          },
        ]}
      />
      <hr id="seo" className="padding--md" />
      <Markdown data={data.seo} />
      <hr id="text-area" className="padding--md" />
      <Markdown data={data.textArea} />
      <TextArea
        id="test"
        blurCallback={v => console.log(v)}
        callback={() => true}
        rows="1"
        title="test"
      />
      <hr id="stateDropdown" className="padding--md" />
      <Markdown data={data.stateDropdown} />
      <StateDropdown
        callback={v => stateValueSetter(v)}
        fullWidth
        id="select--someId"
        title="Some Title"
        optional
        populatedValue={stateValue}
        showFullName
      />
      <hr id="tooltip" className="padding--md" />
      <Markdown data={data.tooltip} />
      <Tooltip
        callback={() => console.log('callback')}
        content={
          <strong>
            {"This is all stuff and it's super duper cool. "}
            <br />
            <br />
            {
              'You can customize how it looks with classes, or just leave it as is and it will default to this style with our sass styles installed'
            }
          </strong>
        }
        id="myId"
        theme="font--dark"
        title="Hello"
        width={300}
      />
      <hr id="validate-email" className="padding--md" />
      <Markdown data={data.validateEmail} />
    </DocsLayout>
  );
};

export default Components;
