import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';

import { getCurrentId } from '../utils/scrollHelpers';

import Borders from '../components/Common/Borders';
import Buttons from '../components/Common/Buttons';
import Colors from '../components/Common/Colors';
import DocsLayout from '../components/Layouts/DocsLayout';
import Grids from '../components/Common/Grids';
import Helpers from '../components/Common/Helpers';
import Images from '../components/Common/Images';
import Markdown from '../components/Common/Markdown';
import RepoLinks from '../components/Common/RepoLinks';
import Shadows from '../components/Common/Shadows';
import Typography from '../components/Common/Typography';

const Styles = ({ location }) => {
  const data = useStaticQuery(graphql`
    {
      installIt: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--install.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      useIt: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--useIt.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      reset: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--reset.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      colors: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--colors.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      customize: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--customize.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      borders: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--borders.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      buttons: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--buttons.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      grid: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--grid.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      helpers: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--helpers.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      images: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--images.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      typography: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--typography.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }

      shadows: allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/styles--shadows.md/" } }
      ) {
        nodes {
          html
          frontmatter {
            description
            html
            slug
            title
          }
        }
      }
    }
  `);

  getCurrentId();

  return (
    <DocsLayout location={location}>
      <RepoLinks />
      <Markdown data={data.installIt} />
      <Markdown data={data.useIt} />
      <Markdown data={data.reset} />
      <Markdown data={data.colors} />
      <Colors />
      <Markdown data={data.customize} />
      <Markdown data={data.borders} />
      <Borders />
      <Markdown data={data.buttons} />
      <Buttons />
      <Markdown data={data.grid} />
      <Grids />
      <Markdown data={data.helpers} />
      <Helpers />
      <Markdown data={data.images} />
      <Images />
      <Markdown data={data.typography} />
      <Typography />
      <Markdown data={data.shadows} />
      <Shadows />
    </DocsLayout>
  );
};

export default Styles;
