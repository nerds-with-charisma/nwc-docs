import React, { useState } from 'react';

import ControlledInput from '../temp/ControlledInput';

const Test = () => {
  const [val, valSetter] = useState('Hello There!');

  return (
    <div>
      <ControlledInput
        autofill
        callback={v => valSetter(v)}
        error="Oops"
        id="myId"
        inputTheme="test-class"
        maxLength={10}
        minLength={10}
        placeHolder="Type something!"
        populatedValue={val}
        require
        theme="test-class-label"
        title="First Name"
        type="text"
        value={val}
      />

      <button type="button" onClick={() => valSetter('Button Clicked')}>
        Update me
      </button>
    </div>
  );
};

export default Test;
