import React, { useEffect, useState } from 'react';

import { Input, TextArea } from '@nerds-with-charisma/nerds-style-sass';

const FantasyFootball = ({ location }) => {
  const [leagueName] = useState(
    location.search.replace('-', ' ').split('?')[1]
  );
  // const [numOfTeams, numOfTeamsSetter] = useState(12);
  const [teams, teamsSetter] = useState();

  const waffleTeams = [
    {
      name: 'Brian D',
      paid: true,
      keeper: 'CmC - RB - Rd 1 (Last year as keeper)',
      isActive: true,
      email: 'briandausman@gmail.com',
      lastYearPlace: null,
      teamName: 'Awful Waffle',
      draftPosition: 12,
    },
    {
      name: 'Bill K',
      paid: true,
      keeper: 'Dak Prescott - QB - Rd 9',
      isActive: true,
      email: 'bkrause102@gmail.com',
      lastYearPlace: '2nd',
      teamName: 'AndywantstobangJenn',
      draftPosition: 11,
    },
    {
      name: 'Travis L',
      paid: true,
      keeper: 'Aaron Jones - RB - Rd 6',
      isActive: true,
      email: 'tralennon@gmail.com',
      lastYearPlace: null,
      teamName: 'IvanDragosDeathPunch',
      draftPosition: 8,
    },
    {
      name: 'Ray G',
      paid: true,
      keeper: 'Michael Thomas - WR - Rd 11',
      isActive: true,
      email: 'rgnaster@gmail.com',
      lastYearPlace: '1st',
      teamName: 'Beastly Bacon',
      draftPosition: 2,
    },
    {
      name: 'Andrew J',
      paid: true,
      keeper: 'Cooper Kupp - WR - Rd 4',
      isActive: true,
      email: 'andrew.jacobs1@gmail.com',
      lastYearPlace: '3rd',
      teamName: 'Terrible Toast',
      draftPosition: 10,
    },
    {
      name: 'Matt B',
      paid: true,
      keeper: 'Nick Chubb - RB - Rd 7',
      isActive: true,
      email: 'mbluems9130@yahoo.com',
      lastYearPlace: null,
      teamName: 'Dr. Matt',
      draftPosition: 3,
    },
    {
      name: 'Chris R',
      paid: true,
      keeper: 'Paddy Mahomes - QB - Rd 10',
      isActive: true,
      email: 'cdreed22@gmail.com',
      lastYearPlace: null,
      teamName: 'The Urban Burrito',
      draftPosition: 6,
    },
    {
      name: 'Jim T',
      paid: true,
      keeper: 'Alvin Kamara - RB - Rd 12',
      isActive: true,
      email: 'jamestongue63@gmail.com',
      lastYearPlace: null,
      teamName: 'Krusty Krab',
      draftPosition: 5,
    },
    {
      name: 'Sharif',
      paid: true,
      keeper: 'Delvin Cook - RB - Rd 1 (Last year as keeper)',
      isActive: true,
      email: 'jamendt@yahoo.com',
      lastYearPlace: null,
      teamName: 'TheMFsheriff',
      draftPosition: 7,
    },
    {
      name: 'Matt M',
      paid: true,
      keeper: 'Kyler Murray - QB - Rd 10',
      isActive: true,
      email: 'moon-m@comcast.net',
      lastYearPlace: null,
      teamName: 'Team Moon',
      draftPosition: 9,
    },
    {
      name: 'Scott B',
      paid: true,
      keeper: 'James Connor - RB - Rd 9',
      isActive: true,
      email: 'scottbluemlein@gmail.com',
      lastYearPlace: null,
      teamName: 'IsItHockeySeasonYet?',
      draftPosition: 4,
    },
    {
      name: 'Angie D',
      paid: true,
      keeper: 'AJ Brown - WR - Rd 10 (Waiver Pickup)',
      isActive: true,
      email: 'angelatongue@gmail.com',
      lastYearPlace: null,
      teamName: 'Nae Nae',
      draftPosition: 1,
    },
  ];

  const cutlerTeams = [
    {
      name: 'Brian D',
      paid: true,
      keeper: 'Aaron Jones - RB - Rd 1',
      isActive: true,
      email: 'briandausman@gmail.com',
      lastYearPlace: null,
      teamName: 'Jay Watch',
      draftPosition: 3,
    },
    {
      name: 'Ron C',
      paid: false,
      keeper: 'Michael Thomas - WR - Rd 1 (Last year as keeper)',
      isActive: true,
      email: 'dabears2769@yahoo.com',
      lastYearPlace: '1st',
      teamName: 'ENDZONE MAFIA',
      draftPosition: 7,
    },
    {
      name: 'David M',
      paid: false,
      keeper: 'CmC - RB - Rd 1 (Last year as keeper)',
      isActive: true,
      email: 'dpmong@gmail.com',
      lastYearPlace: '2nd',
      teamName: 'Ballers!',
      draftPosition: 6,
    },
    {
      name: 'Sharif',
      paid: true,
      keeper: 'Sequon Barkley - RB - Rd 1 (Last year as keeper)',
      isActive: true,
      email: 'jamendt@yahoo.com',
      lastYearPlace: null,
      teamName: "Jeff's Teams",
      draftPosition: 2,
    },
    {
      name: 'Angie D',
      paid: true,
      keeper: 'Kenny Golladay - WR - Rd 4',
      isActive: true,
      email: 'angelatongue@gmail.com',
      lastYearPlace: '3rd',
      teamName: 'Finny Farper',
      draftPosition: 10,
    },
    {
      name: 'Bill K',
      paid: true,
      keeper: 'Nick Chubb - RB - Rd 9',
      isActive: true,
      email: 'bkrause102@gmail.com',
      lastYearPlace: null,
      teamName: 'Max Power',
      draftPosition: 8,
    },
    {
      name: 'Travis L',
      paid: true,
      keeper: 'Lamar Jackson - QB - Rd 12',
      isActive: true,
      email: 'tralennon@gmail.com',
      lastYearPlace: null,
      teamName: 'Jaguar Sharks',
      draftPosition: 11,
    },
    {
      name: 'Kyle L',
      paid: true,
      keeper: 'Alvin Kamara - RB - Rd 1 (Last year can be kept)',
      isActive: true,
      email: 'klindquist1515@yahoo.com',
      lastYearPlace: null,
      teamName: 'Made in Kamarica',
      draftPosition: 5,
    },
    {
      name: 'Ben K',
      paid: true,
      keeper: 'Derrick Henry - RB - Rd 2',
      isActive: true,
      email: 'bnrkeith@gmail.com',
      lastYearPlace: null,
      teamName: 'My team sucks',
      draftPosition: 4,
    },
    {
      name: 'Chris R',
      paid: true,
      keeper: 'Chris Godwin - WR - 4',
      isActive: true,
      email: 'cdreed22@gmail.com',
      lastYearPlace: null,
      teamName: 'The Urban Burrito',
      draftPosition: 1,
    },
    {
      name: 'Jim T  ',
      paid: true,
      keeper: 'Patrick Mahomes - QB - 10',
      isActive: true,
      email: '',
      lastYearPlace: null,
      teamName: 'Chum Bucket',
      draftPosition: 9,
    },
    {
      name: 'David R',
      paid: false,
      keeper: 'Kirk Cousins - QB - Rd 8',
      isActive: true,
      email: 'davidruzga@gmail.com',
      lastYearPlace: null,
      teamName: 'Bears’ Baptisms',
      draftPosition: 12,
    },
  ];

  const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }

    teamsSetter(array);
  };

  useEffect(() => {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'https://kit.fontawesome.com/c817e1a82f.js';
    document.head.appendChild(s);

    shuffleArray(leagueName === 'awful waffle' ? waffleTeams : cutlerTeams);
  }, []);

  return (
    <main className="container--sm bg--offLight">
      <section id="draftOrder" className="padding--md">
        <br />
        <div className="row">
          <div className="col-12">
            <h1 className="margin--bottom">
              <strong>Finalized Draft Order</strong>
            </h1>
            {teams &&
              teams
                .sort((a, b) => (a.draftPosition > b.draftPosition ? 1 : -1))
                .map((team, index) => (
                  <>
                    <div className="bg--light padding--sm font--14 radius--sm shadow--sm">
                      <span className="font--40 float--right">
                        <i
                          class={
                            team.paid
                              ? 'fas fa-money-bill-alt font--success'
                              : 'far fa-frown font--grey'
                          }
                        ></i>
                      </span>

                      {`Pick ${index + 1}`}
                      <br />
                      <a
                        href={`mailto:${team.email}`}
                        className="font--dark font--16"
                      >
                        <strong>{`${team.teamName} `}</strong>
                      </a>
                      {`${
                        team.lastYearPlace !== null
                          ? `:: ${team.lastYearPlace}`
                          : ''
                      }
                `}
                      <br />
                      {team.name}
                      <br />
                      <br />
                      {`Keeper: ${team.keeper || 'NEED IT, DAMNIT'}`}
                      <br />
                    </div>
                    <br />
                  </>
                ))}
          </div>
          {/* <div className="col-12">
            <h1>Random Draft Order Generator</h1>
            {teams &&
              teams.map((team, index) => (
                <div>{`${index + 1}.) ${team.teamName || 'Enter a team'}`}</div>
              ))}
          </div> */}
        </div>

        <br />
        <br />
      </section>
    </main>
  );
};

export default FantasyFootball;
