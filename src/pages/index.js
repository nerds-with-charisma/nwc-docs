import React from 'react';

import { statics } from '../utils/statics';

import Footer from '../components/Footer/Footer';
import GettingStarted from '../components/Home/GettingStarted';
import Header from '../components/Header/Header';
import Hero from '../components/Home/Hero';
import VersionInfo from '../components/Home/VersionInfo';

const IndexPage = () => (
  <main>
    <Header statics={statics} />
    <Hero statics={statics} />

    <section className="row col-12" id="getting--started">
      <GettingStarted statics={statics} />
      <VersionInfo statics={statics} />
    </section>

    <Footer statics={statics} />
  </main>
);

export default IndexPage;
